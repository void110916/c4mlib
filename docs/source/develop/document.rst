文檔建置及修改方式
******************

| 主寫者： `LiYu87 <https://gitlab.com/mickey9910326>`_
| 編輯者：
| 日期：

本專案文件使用 Sphinx 組成，並透過[readthedocs](https://readthedocs.org/)來發佈到網路上，發布網頁： https://readthedocs.org/projects/c4mlib/ 。

[Sphinx 使用手册](https://zh-sphinx-doc.readthedocs.io/en/latest/contents.html)
