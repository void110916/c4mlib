/**
 * @file spi.c
 * @author LiYu87
 * @brief ASA M128 之ASABUS SPI 之硬體初始化。
 * @date 2019.10.13
 */

#include "c4mlib/asabus/src/asabus.h"
#include "c4mlib/asabus/src/pin_def.h"
#include "c4mlib/macro/src/bits_op.h"
#include "c4mlib/macro/src/std_res.h"

#include <avr/io.h>

void ASABUS_SPI_init(void) {
    // set mosi, sck as outputs
    // set miso as inputs
    REGFPT(BUS_SPI_DDR, BUS_SPI_MASK, BUS_SPI_SHIFT, BUS_SPI_OUT);
    // Enable SPI
    // SCK 頻率: fosc/64
    // CPOL = 0，前緣為上升緣。
    // CPHA = 0，取樣時間為前緣。
    SPCR = _BV(SPE) | _BV(MSTR) | _BV(SPR1) | _BV(SPR0);
    SPSR = _BV(SPI2X);
}

char ASABUS_SPI_swap(char data) {
    SPDR = data;
    while (!(SPSR & (1 << SPIF)))
        ;
    return SPDR;
}
