/**
 * @file stp00.h
 * @author s915888
 * @author Yee-Moo
 * @date 2019.09.30
 * @brief stp00函式原型。
 */

#ifndef C4MLIB_ASASTP00_STP00_H
#define C4MLIB_ASASTP00_STP00_H

#include <stdint.h>

/**
 * @defgroup asastp00_func asastp00 functions
 */

/* Public Section Start */
/**
 * @brief 發送資料至stp00
 * @ingroup asastp00_func
 * @param ASA_ID ASA裝置上旋鈕數值
 * @param RegAdd 暫存器位址代號
 *   - 1: 速度設定暫存器。
 *   - 2: 步進步數暫存器。
 *   - 3: 降速除頻值暫存器。
 * @param Bytes 欲發送資料大小 (只能為2bytes)
 * @param Data_p 欲發送資料的變數位址
 * @param Str_p 配合外掛函式群傳參規格
 * @return char 錯誤代碼：
 *   - 0: 正常執行
 *   - 4: ASA_ID錯誤，確認是否小於7
 *   - 7: RegAdd錯誤，確認是否為1~3
 *   - 8: Bytes錯誤，確認是否為2
 *
 * 此函式為c4mlib函式庫內部使用，不開放給使用者使用。
 *
 * 透過ASA_ID選擇ASABUS上指定的裝置。
 * 將Data_p的內容透過spi通訊傳至stp00上指定的暫存器中。
 */
char ASA_STP00_trm(char ASA_ID, char RegAdd, char Bytes, void* Data_p,
                   void* Str_p);

/**
 * @brief 配合外掛函式群規格
 *
 * @ingroup asastp00_func
 * @param ASA_ID 配合外掛函式群傳參規格
 * @param RegAdd 配合外掛函式群傳參規格
 * @param Bytes 配合外掛函式群傳參規格
 * @param Data_p 配合外掛函式群傳參規格
 * @param Str_p 配合外掛函式群傳參規格
 * @return char 錯誤代碼：
 *   - 0: 正常執行
 *
 * 此函式為c4mlib函式庫內部使用，不開放給使用者使用。
 *
 * 內容未實作
 */
char ASA_STP00_rec(char ASA_ID, char RegAdd, char Bytes, void* Data_p,
                   void* Str_p);

/**
 * @brief 配合外掛函式群規格
 *
 * @ingroup asastp00_func
 * @param ASA_ID 配合外掛函式群傳參規格
 * @param RegAdd 配合外掛函式群傳參規格
 * @param Mask 配合外掛函式群傳參規格
 * @param Shift 配合外掛函式群傳參規格
 * @param Data_p 配合外掛函式群傳參規格
 * @param Str_p 配合外掛函式群傳參規格
 * @return char char 錯誤代碼：
 *   - 0: 正常執行
 *
 * 此函式為c4mlib函式庫內部使用，不開放給使用者使用。
 *
 * 內容未實作
 */
char ASA_STP00_frc(char ASA_ID, char RegAdd, char Mask, char Shift,
                   void* Data_p, void* Str_p);

/**
 * @brief 配合外掛函式群規格
 *
 * @ingroup asastp00_func
 * @param ASA_ID 配合外掛函式群傳參規格
 * @param RegAdd 配合外掛函式群傳參規格
 * @param Mask 配合外掛函式群傳參規格
 * @param Shift 配合外掛函式群傳參規格
 * @param Data_p 配合外掛函式群傳參規格
 * @param Str_p 配合外掛函式群傳參規格
 * @return char char 錯誤代碼：
 *   - 0: 正常執行
 *
 * 此函式為c4mlib函式庫內部使用，不開放給使用者使用。
 *
 * 內容未實作
 */
char ASA_STP00_ftm(char ASA_ID, char RegAdd, char Mask, char Shift,
                   void* Data_p, void* Str_p);
/* Public Section End */

#endif  // C4MLIB_ASASTP00_STP00_H
