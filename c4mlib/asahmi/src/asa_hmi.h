/**
 * @file asa_hmi.h
 * @author LiYu87
 * @date 2019.09.25
 * @brief 放置HMI函式原型及Macro
 */

#ifndef C4MLIB_HMI_ASA_HMI_H
#define C4MLIB_HMI_ASA_HMI_H

/**
 * @defgroup asahmi_macro asahmi macros
 * @defgroup asahmi_func  asahmi functions
 */

/* Public Section Start */
// HMI macro
#define HMI_TYPE_I8 \
    0  ///< 資料型態編號 0，int8_t、char                @ingroup asahmi_macro
#define HMI_TYPE_I16 \
    1  ///< 資料型態編號 1，int16_t、int                @ingroup asahmi_macro
#define HMI_TYPE_I32 \
    2  ///< 資料型態編號 2，int32_t、long int           @ingroup asahmi_macro
#define HMI_TYPE_I64 \
    3  ///< 資料型態編號 3，int64_t                     @ingroup asahmi_macro
#define HMI_TYPE_UI8 \
    4  ///< 資料型態編號 4，uint8_t、unsigned char      @ingroup asahmi_macro
#define HMI_TYPE_UI16 \
    5  ///< 資料型態編號 5，uint16_t、unsigned int      @ingroup asahmi_macro
#define HMI_TYPE_UI32 \
    6  ///< 資料型態編號 6，uint32_t、unsigned long int @ingroup asahmi_macro
#define HMI_TYPE_UI64 \
    7  ///< 資料型態編號 7，uint64_t                    @ingroup asahmi_macro
#define HMI_TYPE_F32 \
    8  ///< 資料型態編號 8，float、double               @ingroup asahmi_macro
#define HMI_TYPE_F64 \
    9  ///< 資料型態編號 9，AVR不支援64位元浮點數        @ingroup asahmi_macro

// HMI declaration
/**
 * @brief 發送陣列(1D)到HMI。
 *
 * @ingroup asahmi_func
 * @param Type 陣列的資料型態編號，詳見資料型態對應編號表。
 * @param Num 陣列的個數。
 * @param Data_p 存放陣列的起始記憶體位置，會依序寫入此記憶體後續的資料。
 * @return char 錯誤代碼：：
 *   - 0：成功無誤。
 *   - 1：資料總大小超過30000，請自行切分，並發送。
 *
 * 透過開發版上與PC的接口發送一維陣列資料，發送以 Data_p 為記憶體開頭的陣列資料
 * ，通常為一維的C陣列，會讀取 Num * 形態大小 Bytes的記憶體，若存取記憶體大小超
 * 過Data_p大小，則會發生記憶體非法操作，進而產生錯誤，所以務必確認參數 Num
 * 與實際陣列的個數吻和。
 */
char HMI_put_array(char Type, char Num, void *Data_p);

/**
 * @brief 從HMI接收陣列(1D)。
 *
 * @ingroup asahmi_func
 * @param Type 陣列的資料型態編號，詳見資料型態對應編號表。
 * @param Num 陣列的資料個數。
 * @param Data_p 陣列的起始記憶體位置，會依序讀取此記憶體後續的資料，並送出。
 * @return char 錯誤代碼：：
 *   - 0：成功無誤。
 *   - 1：封包頭錯誤，請檢察通訊雙方流程是否對應。
 *   - 2：封包類型對應錯誤，接收到的資料封包非陣列封包，請檢察通訊雙方流程是否對應。
 *   - 3：封包檢查碼錯誤，請檢查通訊線材品質，並重新發送一次。
 *   - 4：資料型態編號對應錯誤，發送端與接收端的資料型態編號不吻合。
 *   - 5：陣列個數對應錯誤，發送端與接收端的資料個數不吻合。
 *
 * 透過開發版上與PC的接口接收一維陣列資料，放入記憶體位置 Data_p，並會檢查封包類
 * 型、資料型態、及數量等參數。若回傳帶碼不為0，則代表接收失敗，並不會對Data_p
 * 進行寫入。若回傳帶碼為0，會把資料放入Data_p中，請確保Data_p的指標型態與要接
 * 收的資料一致、或足夠容納傳送而來資資料。若Data_p可用記憶體大小小於送來資料大
 * 小，則會發生記憶體非法操作，進而產生錯誤。
 */
char HMI_get_array(char Type, char Num, void *Data_p);

/**
 * @brief 發送矩陣(2D)到HMI。
 *
 * @ingroup asahmi_func
 * @param Type 矩陣的資料型態編號，詳見資料型態對應編號表。
 * @param Dim1 矩陣的維度1大小。
 * @param Dim2 矩陣的維度2大小。
 * @param Data_p 矩陣的起始記憶體位置，會依序讀取此記憶體後續的資料，並送出。
 * @return char 錯誤代碼： 
 *   - 0：成功無誤。
 *   - 1：資料總大小超過30000，請自行切分，並發送。
 *
 * 透過開發版上與PC的接口發送二維矩陣資料，發送以 Data_p 為記憶體開頭的矩陣資料
 * ，通常為二維的C陣列，會讀取 Dim1 * Dim2 * 形態大小 Bytes的記憶體，若存取記
 * 憶體大小超過Data_p大小，則會發生記憶體非法操作，進而產生錯誤，所以務必確認參
 * 數Dim1、Dim2與實際矩陣的個數吻和。
 */
char HMI_put_matrix(char Type, char Dim1, char Dim2, void *Data_p);

/**
 * @brief 從HMI接收矩陣(2D)。
 *
 * @ingroup asahmi_func
 * @param Type 矩陣的資料型態編號，詳見資料型態對應編號表。
 * @param Dim1 矩陣的維度1大小。
 * @param Dim2 矩陣的維度2大小。
 * @param Data_p 存放矩陣的起始記憶體位置，會依序寫入此記憶體後續的資料。
 * @return char 錯誤代碼： 
 *   - 0：成功無誤。
 *   - 1：封包頭錯誤，請檢察通訊雙方流程是否對應。
 *   - 2：封包類型對應錯誤，接收到的資料封包非矩陣封包，請檢察通訊雙方流程是否對應。
 *   - 3：封包檢查碼錯誤，請檢查通訊線材品質，並重新發送一次。
 *   - 4：資料型態編號對應錯誤，發送端與接收端的資料型態編號不吻合。
 *   - 5：矩陣維度一對應錯誤，發送端與接收端的維度一大小不吻合。
 *   - 6：矩陣維度二對應錯誤，發送端與接收端的維度二大小不吻合。
 *
 * 透過開發版上與PC的接口接收二維矩陣資料，放入記憶體位置 Data_p，並會檢查封包類
 * 型、資料型態、及數量等參數。若回傳帶碼不為0，則代表接收失敗，並不會對Data_p
 * 進行寫入。若回傳帶碼為0，會把資料放入Data_p中，請確保Data_p的指標型態與要接
 * 收的資料一致、或足夠容納傳送而來資資料。若Data_p可用記憶體大小小於送來資料大
 * 小，則會發生記憶體非法操作，進而產生錯誤。
 */
char HMI_get_matrix(char Type, char Dim1, char Dim2, void *Data_p);

/**
 * @brief 發送結構到HMI。
 *
 * @ingroup asahmi_func
 * @param FormatString 代表此結構格式的格式字串，詳見FormatString 格式字串。
 * @param Bytes 結構的大小(bytes)。
 * @param Data_p 結構的起始記憶體位置，會依序讀取此記憶體後續的資料，並送出。
 * @return char  錯誤代碼： 
 *   - 0：成功無誤。
 *   - 1：資料總大小超過30000，請自行切分，並發送。
 *
 * 透過開發版上與PC的接口發送結構資料，發送以 Data_p 為記憶體開頭的結構資料，
 * 會讀取 Bytes 大小的記憶體，若存取記體大小超過Data_p大小，則會發生記憶體非
 * 法操作，進而產生錯誤，所以務必確認參數Bytes與實際結構的大小吻和。
 */
char HMI_put_struct(const char *FormatString, int Bytes, void *Data_p);

/**
 * @brief 從HMI接收結構。
 *
 * @ingroup asahmi_func
 * @param FormatString 代表此結構格式的格式字串，詳見FormatString 格式字串。
 * @param Bytes 結構的大小(bytes)。
 * @param Data_p 存放結構的起始記憶體位置，會依序寫入此記憶體後續的資料。
 * @return char 錯誤代碼： 
 *   - 0：成功無誤。
 *   - 1：封包頭錯誤，請檢察通訊雙方流程是否對應。
 *   - 2：封包類型對應錯誤，接收到的資料封包非矩陣封包，請檢察通訊雙方流程是否對應。
 *   - 3：封包檢查碼錯誤，請檢查通訊線材品質，並重新發送一次。
 *   - 4：結構格式字串對應錯誤，發送端與接收端的結構格式字串不吻合。
 *   - 5：結構大小對應錯誤，發送端與接收端的結構大小不吻合。
 *
 * 透過開發版上與PC的接口發送結構資料，發送以 Data_p 為記憶體開頭的結構資料，
 * 會讀取 Bytes 大小的記憶體，若存取記體大小超過Data_p大小，則會發生記憶體非
 * 法操作，進而產生錯誤，所以務必確認參數Bytes與實際結構的大小吻和。
 */
char HMI_get_struct(const char *FormatString, int Bytes, void *Data_p);

/**
 * @brief 主動同步後，發送陣列(1D)到HMI。
 *
 * @ingroup asahmi_func
 * @param Type 陣列的資料型態編號，詳見資料型態對應編號表。
 * @param Num 陣列的個數。
 * @param Data_p 存放陣列的起始記憶體位置，會依序寫入此記憶體後續的資料。
 * @return char 錯誤代碼： 
 *   - 0：成功無誤。
 *   - 1：資料總大小超過30000，請自行切分，並發送。
 *   - 7：同步失敗。
 *
 * 發送同步請求與HMI端進行同步動作，同步成功後開始從HMI接收結構，若同步失敗則不
 * 進行後續動作。
 * 透過開發版上與PC的接口發送一維陣列資料，發送以 Data_p 為記憶體開頭的陣列資料
 * ，通常為一維的C陣列，會讀取 Num * 形態大小 Bytes的記憶體，若存取記憶體大小超
 * 過Data_p大小，則會發生記憶體非法操作，進而產生錯誤，所以務必確認參數 Num
 * 與實際陣列的個數吻和。
 */
char HMI_snput_array(char Type, char Num, void *Data_p);

/**
 * @brief 主動同步後，從HMI接收陣列(1D)。
 *
 * @ingroup asahmi_func
 * @param Type 陣列的資料型態編號，詳見資料型態對應編號表。
 * @param Num 陣列的資料個數。
 * @param Data_p 陣列的起始記憶體位置，會依序讀取此記憶體後續的資料，並送出。
 * @return char 錯誤代碼： 
 *   - 0：成功無誤。
 *   - 1：封包頭錯誤，請檢察通訊雙方流程是否對應。
 *   - 2：封包類型對應錯誤，接收到的資料封包非陣列封包，請檢察通訊雙方流程是否對應。
 *   - 3：封包檢查碼錯誤，請檢查通訊線材品質，並重新發送一次。
 *   - 4：資料型態編號對應錯誤，發送端與接收端的資料型態編號不吻合。
 *   - 5：陣列個數對應錯誤，發送端與接收端的資料個數不吻合。
 *   - 7：同步失敗。
 *
 * 發送同步請求與HMI端進行同步動作，同步成功後開始從HMI接收結構，若同步失敗則不
 * 進行後續動作。<br>
 * 透過開發版上與PC的接口接收一維陣列資料，放入記憶體位置 Data_p，並會檢查封包類
 * 型、資料型態、及數量等參數。若回傳帶碼不為0，則代表接收失敗，並不會對Data_p
 * 進行寫入。若回傳帶碼為0，會把資料放入Data_p中，請確保Data_p的指標型態與要接
 * 收的資料一致、或足夠容納傳送而來資資料。若Data_p可用記憶體大小小於送來資料大
 * 小，則會發生記憶體非法操作，進而產生錯誤。
 */
char HMI_snget_array(char Type, char Num, void *Data_p);

/**
 * @brief 主動同步後，發送矩陣(2D)到HMI。
 *
 * @ingroup asahmi_func
 * @param Type 矩陣的資料型態編號，詳見資料型態對應編號表。
 * @param Dim1 矩陣的維度1大小。
 * @param Dim2 矩陣的維度2大小。
 * @param Data_p 矩陣的起始記憶體位置，會依序讀取此記憶體後續的資料，並送出。
 * @return char 錯誤代碼：
 *   - 0：成功無誤。
 *   - 1：資料總大小超過30000，請自行切分，並發送。
 *   - 7：同步失敗。
 *
 * 發送同步請求與HMI端進行同步動作，同步成功後開始從HMI接收結構，若同步失敗則不
 * 進行後續動作。<br>
 * 透過開發版上與PC的接口發送二維矩陣資料，發送以 Data_p 為記憶體開頭的矩陣資料
 * ，通常為二維的C陣列，會讀取 Dim1 * Dim2 * 形態大小 Bytes的記憶體，若存取記
 * 憶體大小超過Data_p大小，則會發生記憶體非法操作，進而產生錯誤，所以務必確認參
 * 數Dim1、Dim2與實際矩陣的個數吻和。
 */
char HMI_snput_matrix(char Type, char Dim1, char Dim2, void *Data_p);

/**
 * @brief 主動同步後，從HMI接收矩陣(2D)。
 *
 * @ingroup asahmi_func
 * @param Type 矩陣的資料型態編號，詳見資料型態對應編號表。
 * @param Dim1 矩陣的維度1大小。
 * @param Dim2 矩陣的維度2大小。
 * @param Data_p 存放矩陣的起始記憶體位置，會依序寫入此記憶體後續的資料。
 * @return char 錯誤代碼： 
 *   - 0：成功無誤。
 *   - 1：封包頭錯誤，請檢察通訊雙方流程是否對應。
 *   - 2：封包類型對應錯誤，接收到的資料封包非矩陣封包，請檢察通訊雙方流程是否對應。
 *   - 3：封包檢查碼錯誤，請檢查通訊線材品質，並重新發送一次。
 *   - 4：資料型態編號對應錯誤，發送端與接收端的資料型態編號不吻合。
 *   - 5：矩陣維度一對應錯誤，發送端與接收端的維度一大小不吻合。
 *   - 6：矩陣維度二對應錯誤，發送端與接收端的維度二大小不吻合。
 *   - 7：同步失敗。
 *
 * 發送同步請求與HMI端進行同步動作，同步成功後開始從HMI接收結構，若同步失敗則不
 * 進行後續動作。<br>
 * 透過開發版上與PC的接口接收二維矩陣資料，放入記憶體位置 Data_p，並會檢查封包類
 * 型、資料型態、及數量等參數。若回傳帶碼不為0，則代表接收失敗，並不會對Data_p
 * 進行寫入。若回傳帶碼為0，會把資料放入Data_p中，請確保Data_p的指標型態與要接
 * 收的資料一致、或足夠容納傳送而來資資料。若Data_p可用記憶體大小小於送來資料大
 * 小，則會發生記憶體非法操作，進而產生錯誤。
 */
char HMI_snget_matrix(char Type, char Dim1, char Dim2, void *Data_p);

/**
 * @brief 主動同步後，發送結構到HMI。
 *
 * @ingroup asahmi_func
 * @param FormatString 代表此結構格式的格式字串，詳見FormatString 格式字串。
 * @param Bytes 結構的大小(bytes)。
 * @param Data_p 結構的起始記憶體位置，會依序讀取此記憶體後續的資料，並送出。
 * @return char  錯誤代碼： <br>
 *   - 0：成功無誤。<br>
 *   - 1：資料總大小超過30000，請自行切分，並發送。<br>
 *   - 7：同步失敗。
 *
 * 發送同步請求與HMI端進行同步動作，同步成功後開始從HMI接收結構，若同步失敗則不
 * 進行後續動作。<br>
 * 透過開發版上與PC的接口發送結構資料，發送以 Data_p 為記憶體開頭的結構資料，
 * 會讀取 Bytes 大小的記憶體，若存取記體大小超過Data_p大小，則會發生記憶體非
 * 法操作，進而產生錯誤，所以務必確認參數Bytes與實際結構的大小吻和。
 */
char HMI_snput_struct(const char *FormatString, int Bytes, void *Data_p);

/**
 * @brief 主動同步後，從HMI接收結構。
 *
 * @ingroup asahmi_func
 * @param FormatString 代表此結構格式的格式字串，詳見FormatString 格式字串。
 * @param Bytes 結構的大小(bytes)。
 * @param Data_p 存放結構的起始記憶體位置，會依序寫入此記憶體後續的資料。
 * @return char  錯誤代碼： <br>
 *   - 0：成功無誤。<br>
 *   - 1：封包頭錯誤，請檢察通訊雙方流程是否對應。<br>
 *   - 2：封包類型對應錯誤，接收到的資料封包非矩陣封包，請檢察通訊雙方流程是否對應。<br>
 *   - 3：封包檢查碼錯誤，請檢查通訊線材品質，並重新發送一次。<br>
 *   - 4：結構格式字串對應錯誤，發送端與接收端的結構格式字串不吻合。<br>
 *   - 5：結構大小對應錯誤，發送端與接收端的結構大小不吻合。<br>
 *   - 7：同步失敗。
 *
 * 發送同步請求與HMI端進行同步動作，同步成功後開始從HMI接收結構，若同步失敗則不
 * 進行後續動作。<br>
 * 透過開發版上與PC的接口發送結構資料，發送以 Data_p 為記憶體開頭的結構資料，
 * 會讀取 Bytes 大小的記憶體，若存取記體大小超過Data_p大小，則會發生記憶體非
 * 法操作，進而產生錯誤，所以務必確認參數Bytes與實際結構的大小吻和。
 */
char HMI_snget_struct(const char *FormatString, int Bytes, void *Data_p);
/* Public Section End */

#endif  // C4MLIB_HMI_ASA_HMI_H
