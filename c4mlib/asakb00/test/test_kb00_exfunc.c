/**
 * @file test_kb00_exfunc.c
 * @author s915888
 * @brief 測試kb00在外掛函式中的運作。
 * @date 2019.8.29
 */

#include "c4mlib/asaspi/src/asaspi_master.h"
#include "c4mlib/device/src/device.h"
#include "c4mlib/config/asamodule.cfg"

#define DELAY 10

int main() {
    C4M_DEVICE_set();
    char kb00 = 1;
    ASA_SPIM_ftm(100, 4, 200, 0xff, 0, &kb00, DELAY);
    // 設定成鍵號模式

    char rec;
    while (1) {
        ASA_SPIM_rec(100, 4, 100, 1, &rec, DELAY);
        printf("rec=%d\n", rec);
    }
}
