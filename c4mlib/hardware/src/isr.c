/**
 * @file isr.c
 * @author LiYu87
 * @date 2019.07.15
 * @brief c4mlib 硬體中斷常式實現 各硬體實作分割。
 *
 * 將原生硬體中斷常式實作成 4mlib 的中斷，方可使用巨集
 * INTERNAL_ISR 與 ISR。
 */

void USE_C4MLIB_INTERRUPT() {
}

#if defined(__AVR_ATmega128__)
#    include "m128/isr.c"
#elif defined(__AVR_ATmega88__) || defined(__AVR_ATmega48__) || \
    defined(__AVR_ATmega168__)
#    include "m88/isr.c"
#elif defined(__AVR_ATtiny2313__)
#    include "tiny2313/isr.c"
#else
#    if !defined(__COMPILING_AVR_LIBC__)
#        warning "device type not defined"
#    endif
#endif
