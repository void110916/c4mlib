/**
 * @file test_rtpin.c
 * @author Yi-Mou
 * @brief rtpio實作
 * @date 2019-08-19
 *
 * 不使用中斷，只測試step函式作用及函式用法
 */

#include "c4mlib/device/src/device.h"
#include <avr/io.h>

#include "c4mlib/rtpio/src/rtpio.h"

RealTimePortStr_t RealTimePortIn_1;

int main(){

    C4M_DEVICE_set();

    RealTimePort_net(&RealTimePortIn_1,(uint8_t *)&PINA,1);
    uint8_t data = 0;
    while(1){
        RealTimePortIn_step(&RealTimePortIn_1);
        if(RealTimePortIn_1.TrigCount==1) {
            data = RealTimePortIn_1.Buff[0];
             RealTimePortIn_1.TrigCount--;
        }
        else {
             data = data;
        }
        
        printf("data=%d\n", data);
    }
}
