/**
 * @file rtupcount.c
 * @author Yi-Mou
 * @brief reatimeupcount實現
 * @date 2019-08-26
 */

#include "rtupcount.h"
#include <stddef.h>

uint8_t RealTimeUpCount_net(RealTimeUpCountStr_t* Str_p){
    return 0;
}

void RealTimeUpCount_step(RealTimeUpCountStr_t* RealTimeUpCountStr_p){
    RealTimeUpCountStr_p->TrigCount++;
}
