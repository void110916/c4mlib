/**
 * @file spi_int_step.c
 * @author LiYu87
 * @date 2019.09.04
 * @brief
 *
 * NOTE
 * UartTx_step, UartRx_step is used by hardware interrupt
 * and its code is in hardware/std_isr.
 */

#include "c4mlib/hardware2/src/uart.h"

void UartTx_step(UartIntStr_t *IntStr_p) {
    if (IntStr_p != NULL) {
        for (uint8_t i = 0; i < IntStr_p->TxIntTotal; i++) {
            if (IntStr_p->TxIntFb[i].enable) {
                IntStr_p->TxIntFb[i].func_p(IntStr_p->TxIntFb[i].funcPara_p);
            }
        }
    }
}

void UartRx_step(UartIntStr_t *IntStr_p) {
    if (IntStr_p != NULL) {
        for (uint8_t i = 0; i < IntStr_p->RxIntTotal; i++) {
            if (IntStr_p->RxIntFb[i].enable) {
                IntStr_p->RxIntFb[i].func_p(IntStr_p->RxIntFb[i].funcPara_p);
            }
        }
    }
}

