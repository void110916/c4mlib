/**
 * @file extint.h
 * @author Deng Xiang-Guan
 * @date 2019.08.08
 * @brief 提供ASA函式庫標準中斷介面，將原生硬體Interrupt呼叫函式占用，並提供登陸函式介面。
 */

#ifndef C4MLIB_HARDWARE2_EXTINT_H
#define C4MLIB_HARDWARE2_EXTINT_H

#include "c4mlib/interrupt/src/isr_func.h"
#include "c4mlib/macro/src/std_def.h"
#include "c4mlib/macro/src/std_type.h"

#include <stdint.h>

#include "c4mlib/config/interrupt.cfg"

/* Public Section Start */
typedef struct __ext_set_str {
    uint8_t InOut;     ///< 外部中斷腳位的輸出入設定
    uint8_t Mode;      ///< 外部中斷觸發模式
    uint8_t IntEnable; ///< 是否啟用外部中斷
} ExtSetStr_t;

typedef struct __ext_int_str {
    ExtSetStr_t ExtSet;

    uint8_t (*SetFunc_p)(struct __ext_int_str*);

    uint8_t IntTotal;                                    ///< 紀錄已有多少中斷已註冊
    volatile FuncBlockStr_t IntFb[MAX_EXTINT_FUNCNUM];   ///< 紀錄所有已註冊的中斷函式
} ExtIntStr_t;

uint8_t ExtInt_net(ExtIntStr_t* ExtIntStr_p, uint8_t Num);

uint8_t ExtInt_set(ExtIntStr_t* ExtIntStr_p);

uint8_t ExtInt_reg(ExtIntStr_t* ExtIntStr_p, Func_t FbFunc_p, void* FbPara_p);

void ExtInt_en(ExtIntStr_t* ExtIntStr_p, uint8_t Fb_Id, uint8_t enable);

void ExtInt_step(ExtIntStr_t* ExtIntStr_p);
/* Public Section End */

/* Public Section Start */
/*----- Ext Sets Macros -----------------------------------------------------*/
#define EXT_MODE_LOW_LEVEL 0
#define EXT_MODE_FALLING 1
#define EXT_MODE_RAISING 3
/*---------------------------------------------------------------------------*/
/* Public Section End */

#endif  // C4MLIB_HARDWARE2_EXTINT_H
