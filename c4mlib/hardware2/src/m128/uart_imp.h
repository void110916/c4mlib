/**
 * @file uart_imp.h
 * @author 
 * @brief 
 * @date 2019.09.04
 * 
 */

#ifndef C4MLIB_HARDWARE2_M128_UART_IMP_H
#define C4MLIB_HARDWARE2_M128_UART_IMP_H

#include "c4mlib/hardware2/src/uart.h"

/* Public Section Start */
#define UART_HW_NUM 2
extern UartIntStr_t UartImp[UART_HW_NUM];
extern UartIntStr_t* UartTxIntStrList_p[UART_HW_NUM];
extern UartIntStr_t* UartRxIntStrList_p[UART_HW_NUM];
/* Public Section End */

#endif  // C4MLIB_HARDWARE2_M128_UART_IMP_H
