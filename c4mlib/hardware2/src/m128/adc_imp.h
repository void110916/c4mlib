/**
 * @file adc_imp.h
 * @author LiYu87
 * @date 2019.09.04
 * @brief 
 * 
 */

#ifndef C4MLIB_HARDWARE2_M128_ADC_IMP_H
#define C4MLIB_HARDWARE2_M128_ADC_IMP_H

#include "c4mlib/hardware2/src/adc.h"

/* Public Section Start */
#define ADC_HW_NUM 1
extern AdcIntStr_t AdcImp[ADC_HW_NUM];
extern AdcIntStr_t *AdcIntStrList_p[ADC_HW_NUM];
/* Public Section End */

#endif  // C4MLIB_HARDWARE2_M128_ADC_IMP_H
