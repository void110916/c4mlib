/**
 * @file ext_imp.c
 * @author LiYu87
 * @date 2019.11.14
 * @brief
 *
 */

#include "c4mlib/hardware2/src/m128/ext_imp.h"

#include "c4mlib/hardware2/src/extint.h"
#include "c4mlib/hardware2/src/m128/ext_imp.h"
#include "c4mlib/macro/src/bits_op.h"
#include "c4mlib/macro/src/std_def.h"
#include "c4mlib/macro/src/std_res.h"

#include <avr/io.h>

static uint8_t ext0_set(ExtIntStr_t* IntStr_p);
static uint8_t ext1_set(ExtIntStr_t* IntStr_p);
static uint8_t ext2_set(ExtIntStr_t* IntStr_p);
static uint8_t ext3_set(ExtIntStr_t* IntStr_p);
static uint8_t ext4_set(ExtIntStr_t* IntStr_p);
static uint8_t ext5_set(ExtIntStr_t* IntStr_p);
static uint8_t ext6_set(ExtIntStr_t* IntStr_p);
static uint8_t ext7_set(ExtIntStr_t* IntStr_p);

ExtIntStr_t ExtImp[EXT_HW_NUM] = {
    {
        .ExtSet =
            {
                .InOut = INPUT,
                .Mode = EXT_MODE_FALLING,
                .IntEnable = ENABLE,
            },
        .SetFunc_p = ext0_set,
    },
    {
        .ExtSet =
            {
                .InOut = INPUT,
                .Mode = EXT_MODE_FALLING,
                .IntEnable = ENABLE,
            },
        .SetFunc_p = ext1_set,
    },
    {
        .ExtSet =
            {
                .InOut = INPUT,
                .Mode = EXT_MODE_FALLING,
                .IntEnable = ENABLE,
            },
        .SetFunc_p = ext2_set,
    },
    {
        .ExtSet =
            {
                .InOut = INPUT,
                .Mode = EXT_MODE_FALLING,
                .IntEnable = ENABLE,
            },
        .SetFunc_p = ext3_set,
    },
    {
        .ExtSet =
            {
                .InOut = INPUT,
                .Mode = EXT_MODE_FALLING,
                .IntEnable = ENABLE,
            },
        .SetFunc_p = ext4_set,
    },
    {
        .ExtSet =
            {
                .InOut = INPUT,
                .Mode = EXT_MODE_FALLING,
                .IntEnable = ENABLE,
            },
        .SetFunc_p = ext5_set,
    },
    {
        .ExtSet =
            {
                .InOut = INPUT,
                .Mode = EXT_MODE_FALLING,
                .IntEnable = ENABLE,
            },
        .SetFunc_p = ext6_set,
    },
    {
        .ExtSet =
            {
                .InOut = INPUT,
                .Mode = EXT_MODE_FALLING,
                .IntEnable = ENABLE,
            },
        .SetFunc_p = ext7_set,
    },
};

ExtIntStr_t* ExtIntStrList_p[EXT_HW_NUM];

#define IS_EXT_MODE(MODE)                                       \
    (((MODE) == EXT_MODE_LOW_LEVEL) | ((MODE) == EXT_MODE_FALLING) | \
    ((MODE) == EXT_MODE_RAISING))

uint8_t ext0_set(ExtIntStr_t* IntStr_p) {
    if (!IS_INPUT_OR_OUTPUT(IntStr_p->ExtSet.InOut)) {
        return 1;
    }
    if (!IS_EXT_MODE(IntStr_p->ExtSet.Mode)) {
        return 2;
    }
    if (!IS_ENABLE_OR_DISABLE(IntStr_p->ExtSet.IntEnable)) {
        return 3;
    }

    REGFPT(DDRE, 0x01 << 0, 0, IntStr_p->ExtSet.InOut);
    REGFPT(EICRA, 0x03 << 0, 0, IntStr_p->ExtSet.Mode);
    REGFPT(EIMSK, 0x03 << 0, 0, IntStr_p->ExtSet.IntEnable);

    return 0;
}

uint8_t ext1_set(ExtIntStr_t* IntStr_p) {
    if (!IS_INPUT_OR_OUTPUT(IntStr_p->ExtSet.InOut)) {
        return 1;
    }
    if (!IS_EXT_MODE(IntStr_p->ExtSet.Mode)) {
        return 2;
    }
    if (!IS_ENABLE_OR_DISABLE(IntStr_p->ExtSet.IntEnable)) {
        return 3;
    }

    REGFPT(DDRE, 0x01 << 1, 1, IntStr_p->ExtSet.InOut);
    REGFPT(EICRA, 0x03 << 2, 2, IntStr_p->ExtSet.Mode);
    REGFPT(EIMSK, 0x03 << 1, 1, IntStr_p->ExtSet.IntEnable);

    return 0;
}

uint8_t ext2_set(ExtIntStr_t* IntStr_p) {
    if (!IS_INPUT_OR_OUTPUT(IntStr_p->ExtSet.InOut)) {
        return 1;
    }
    if (!IS_EXT_MODE(IntStr_p->ExtSet.Mode)) {
        return 2;
    }
    if (!IS_ENABLE_OR_DISABLE(IntStr_p->ExtSet.IntEnable)) {
        return 3;
    }

    REGFPT(DDRE, 0x01 << 2, 2, IntStr_p->ExtSet.InOut);
    REGFPT(EICRA, 0x03 << 4, 4, IntStr_p->ExtSet.Mode);
    REGFPT(EIMSK, 0x03 << 2, 2, IntStr_p->ExtSet.IntEnable);

    return 0;
}

uint8_t ext3_set(ExtIntStr_t* IntStr_p) {
    if (!IS_INPUT_OR_OUTPUT(IntStr_p->ExtSet.InOut)) {
        return 1;
    }
    if (!IS_EXT_MODE(IntStr_p->ExtSet.Mode)) {
        return 2;
    }
    if (!IS_ENABLE_OR_DISABLE(IntStr_p->ExtSet.IntEnable)) {
        return 3;
    }

    REGFPT(DDRE, 0x01 << 3, 3, IntStr_p->ExtSet.InOut);
    REGFPT(EICRA, 0x03 << 6, 6, IntStr_p->ExtSet.Mode);
    REGFPT(EIMSK, 0x03 << 3, 3, IntStr_p->ExtSet.IntEnable);

    return 0;
}

uint8_t ext4_set(ExtIntStr_t* IntStr_p) {
    if (!IS_INPUT_OR_OUTPUT(IntStr_p->ExtSet.InOut)) {
        return 1;
    }
    if (!IS_EXT_MODE(IntStr_p->ExtSet.Mode)) {
        return 2;
    }
    if (!IS_ENABLE_OR_DISABLE(IntStr_p->ExtSet.IntEnable)) {
        return 3;
    }

    REGFPT(DDRE, 0x01 << 4, 4, IntStr_p->ExtSet.InOut);
    REGFPT(EICRB, 0x03 << 0, 0, IntStr_p->ExtSet.Mode);
    REGFPT(EIMSK, 0x03 << 4, 4, IntStr_p->ExtSet.IntEnable);

    return 0;
}

uint8_t ext5_set(ExtIntStr_t* IntStr_p) {
    if (!IS_INPUT_OR_OUTPUT(IntStr_p->ExtSet.InOut)) {
        return 1;
    }
    if (!IS_EXT_MODE(IntStr_p->ExtSet.Mode)) {
        return 2;
    }
    if (!IS_ENABLE_OR_DISABLE(IntStr_p->ExtSet.IntEnable)) {
        return 3;
    }

    REGFPT(DDRE, 0x01 << 5, 5, IntStr_p->ExtSet.InOut);
    REGFPT(EICRB, 0x03 << 2, 2, IntStr_p->ExtSet.Mode);
    REGFPT(EIMSK, 0x03 << 5, 5, IntStr_p->ExtSet.IntEnable);

    return 0;
}

uint8_t ext6_set(ExtIntStr_t* IntStr_p) {
    if (!IS_INPUT_OR_OUTPUT(IntStr_p->ExtSet.InOut)) {
        return 1;
    }
    if (!IS_EXT_MODE(IntStr_p->ExtSet.Mode)) {
        return 2;
    }
    if (!IS_ENABLE_OR_DISABLE(IntStr_p->ExtSet.IntEnable)) {
        return 3;
    }

    REGFPT(DDRE, 0x01 << 6, 6, IntStr_p->ExtSet.InOut);
    REGFPT(EICRB, 0x03 << 4, 4, IntStr_p->ExtSet.Mode);
    REGFPT(EIMSK, 0x03 << 6, 6, IntStr_p->ExtSet.IntEnable);

    return 0;
}

uint8_t ext7_set(ExtIntStr_t* IntStr_p) {
    if (!IS_INPUT_OR_OUTPUT(IntStr_p->ExtSet.InOut)) {
        return 1;
    }
    if (!IS_EXT_MODE(IntStr_p->ExtSet.Mode)) {
        return 2;
    }
    if (!IS_ENABLE_OR_DISABLE(IntStr_p->ExtSet.IntEnable)) {
        return 3;
    }

    REGFPT(DDRE, 0x01 << 7, 7, IntStr_p->ExtSet.InOut);
    REGFPT(EICRB, 0x03 << 6, 6, IntStr_p->ExtSet.Mode);
    REGFPT(EIMSK, 0x03 << 7, 7, IntStr_p->ExtSet.IntEnable);

    return 0;
}
