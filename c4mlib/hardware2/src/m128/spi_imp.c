/**
 * @file spi_imp.c
 * @author maxwu84
 * @date 2019.09.04
 * @brief 
 * 
 */

#include "c4mlib/hardware2/src/m128/spi_imp.h"

#include "c4mlib/asabus/src/asabus.h"
#include "c4mlib/asabus/src/pin_def.h"
#include "c4mlib/macro/src/bits_op.h"
#include "c4mlib/macro/src/std_def.h"
#include "c4mlib/macro/src/std_res.h"

#include <avr/io.h>

#include "c4mlib/config/spi.cfg"

static uint8_t spi_hw_init(SpiIntStr_t *IntStr_p);

SpiIntStr_t SpiImp[SPI_HW_NUM] = {
    {
        .SpiSet = SPI_HW_SET_CFG,
        .SetFunc_p = spi_hw_init
    }
};

SpiIntStr_t *SpiIntStrList_p[SPI_HW_NUM];

uint8_t spi_hw_init(SpiIntStr_t *IntStr_p) {
    switch(IntStr_p->SpiSet.SynchRdWt){
        case SPI_SYNCHRDWT_FRONTREAD:
            SPCR &= ~(1 << CPHA);
            SPCR |= (0 << CPHA);
            break;
        case SPI_SYNCHRDWT_FRONTWRITE:
            SPCR &= ~(1 << CPHA);
            SPCR |= (1 << CPHA);
            break;
    }
    switch(IntStr_p->SpiSet.SynchRiseDown){
        case SPI_SYNCHRISEDOWN_FRONTRISE:
            SPCR &= ~(1 << CPOL);
            SPCR |= (0 << CPOL);
            break;
        case SPI_SYNCHRISEDOWN_FRONTDOWN:
            SPCR &= ~(1 << CPOL);
            SPCR |= (1 << CPOL);
            break;
    }
    switch(IntStr_p->SpiSet.MSSelect){
        case SPI_MSSELECT_SLAVE:
            //TODO 確認 56~57 是否必要
            DDRB &= ~(1 << BUS_SPI_MOSI) & ~(1 << BUS_SPI_SCK) & ~(1 << BUS_SPI_SS);
            DDRB |= (1 << BUS_SPI_MISO);
            
            SPCR &= ~(1 << MSTR);
            SPCR |= (0 << MSTR);
            break;
            
        case SPI_MSSELECT_MASTER:
            //TODO 確認 65~71 是否必要
            // Initialize ASA SPI ID
            ASABUS_ID_init();
            // Setup SPI pins
            DDRB |= (1 << BUS_SPI_MOSI) | (1 << BUS_SPI_SCK) | (1 << BUS_SPI_SS);
            DDRB &= ~(1 << BUS_SPI_MISO);
            // Master CS pin is PF4
            ASA_CS_DDR |= (1 << ASA_CS);

            SPCR &= ~(1 << MSTR);
            SPCR |= (1 << MSTR);
            break;
    }
    switch(IntStr_p->SpiSet.ByteOrder){
        case SPI_BYTEORDER_LOWFIRST:
            SPCR &= ~(1 << DORD);
            SPCR |= (1 << DORD);
            break;
        case SPI_BYTEORDER_HIFIRST:
            SPCR &= ~(1 << DORD);
            SPCR |= (0 << DORD);
            break;
    }
    switch(IntStr_p->SpiSet.IntEn){
        case DISABLE:
            SPCR &= ~(1 << SPIE);
            SPCR |= (0 << SPIE);
            break;
        case ENABLE:
            SPCR &= ~(1 << SPIE);
            SPCR |= (1 << SPIE);
            break;
    }
    switch(IntStr_p->SpiSet.BitRatePara0){
        case SPI_CLOCLPRESCALEDBY1_4:
            SPCR &= ~((1 << SPR1) | (1 << SPR0));
            SPCR |= ((0 << SPR1) | (0 << SPR0));
            SPSR &= ~(1 << SPI2X);
            SPSR |= (0 << SPI2X);
            break;
        case SPI_CLOCLPRESCALEDBY1_16:
            SPCR &= ~((1 << SPR1) | (1 << SPR0));
            SPCR |= ((0 << SPR1) | (1 << SPR0));
            SPSR &= ~(1 << SPI2X);
            SPSR |= (0 << SPI2X);
            break;
        case SPI_CLOCLPRESCALEDBY1_64:
            SPCR &= ~((1 << SPR1) | (1 << SPR0));
            SPCR |= ((1 << SPR1) | (0 << SPR0));
            SPSR &= ~(1 << SPI2X);
            SPSR |= (0 << SPI2X);
            break;
        case SPI_CLOCLPRESCALEDBY1_128:
            SPCR &= ~((1 << SPR1) | (1 << SPR0));
            SPCR |= ((1 << SPR1) | (1 << SPR0));
            SPSR &= ~(1 << SPI2X);
            SPSR |= (0 << SPI2X);
            break;
        case SPI_CLOCLPRESCALEDBY1_2:
            SPCR &= ~((1 << SPR1) | (1 << SPR0));
            SPCR |= ((0 << SPR1) | (0 << SPR0));
            SPSR &= ~(1 << SPI2X);
            SPSR |= (1 << SPI2X);
            break;
        case SPI_CLOCLPRESCALEDBY1_8:
            SPCR &= ~((1 << SPR1) | (1 << SPR0));
            SPCR |= ((0 << SPR1) | (1 << SPR0));
            SPSR &= ~(1 << SPI2X);
            SPSR |= (1 << SPI2X);
            break;
        case SPI_CLOCLPRESCALEDBY1_32:
            SPCR &= ~((1 << SPR1) | (1 << SPR0));
            SPCR |= ((1 << SPR1) | (0 << SPR0));
            SPSR &= ~(1 << SPI2X);
            SPSR |= (1 << SPI2X);
            break; 
    }
    switch(IntStr_p->SpiSet.BitRatePara1){
        // TODO 看是否 留存，不然就跟 BitRatePara0 併入一起
    }
    switch(IntStr_p->SpiSet.TxRxEn){
        case DISABLE:
            SPCR &= ~(1 << SPE);
            SPCR |= (0 << SPE);
            break;
        case ENABLE:
            SPCR &= ~(1 << SPE);
            SPCR |= (1 << SPE);
            break;
    }

    return 0 ;
}
