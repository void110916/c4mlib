/**
 * @file spi.h
 * @author
 * @date
 * @brief
 * 
 */

#ifndef C4MLIB_HAREWARE2_SPI_H
#define C4MLIB_HAREWARE2_SPI_H

#include "c4mlib/interrupt/src/isr_func.h"
#include "c4mlib/macro/src/std_def.h"
#include "c4mlib/macro/src/std_type.h"

#include <stdint.h>

#include "c4mlib/config/interrupt.cfg"

/* Public Section Start */
typedef struct __spi_set_str {
    uint8_t SynchRdWt;
    uint8_t SynchRiseDown;
    uint8_t MSSelect ;
    uint8_t ByteOrder;
    uint8_t TxRxEn;
    uint8_t IntEn ;
    uint8_t BitRatePara0;
    uint8_t BitRatePara1;
} SpiSetStr_t;

typedef struct __spi_int_str {
    SpiSetStr_t SpiSet;
    
    uint8_t (*SetFunc_p)(struct __spi_int_str*);

    uint8_t IntTotal;                                     ///< 紀錄已有多少中斷已註冊
    volatile FuncBlockStr_t IntFb[MAX_SPIIINT_FUNCNUM];   ///< 紀錄所有已註冊的中斷函式
} SpiIntStr_t;

uint8_t SpiInt_net(SpiIntStr_t* IntStr_p, uint8_t Num);

uint8_t SpiInt_set(SpiIntStr_t* IntStr_p);

uint8_t SpiInt_reg(SpiIntStr_t* IntStr_p, Func_t FbFunc_p, void* FbPara_p);

void SpiInt_en(SpiIntStr_t* IntStr_p, uint8_t Fb_Id, uint8_t enable);

void SpiInt_step(SpiIntStr_t* IntStr_p);
/* Public Section End */

/*----- Pwm Sets Macros -----------------------------------------------------*/
#define DISABLE 0
#define ENABLE 1
/* SynchRdWt*/ 
#define SPI_SYNCHRDWT_FRONTREAD     0
#define SPI_SYNCHRDWT_FRONTWRITE    1
/* SynchRiseDown*/
#define SPI_SYNCHRISEDOWN_FRONTRISE 0
#define SPI_SYNCHRISEDOWN_FRONTDOWN 1
/* MSSelect */
#define SPI_MSSELECT_SLAVE      0
#define SPI_MSSELECT_MASTER     1
/* ByteOrder*/
#define SPI_BYTEORDER_HIFIRST   0
#define SPI_BYTEORDER_LOWFIRST  1
/* TxRxEn*/ // DISABLE, ENABLE
/* IntEn */ // DISABLE, ENABLE
/* BitRatePara0*/
#define SPI_CLOCLPRESCALEDBY0_2     0
#define SPI_CLOCLPRESCALEDBY0_4     1
#define SPI_CLOCLPRESCALEDBY0_8     2
#define SPI_CLOCLPRESCALEDBY0_16    3
#define SPI_CLOCLPRESCALEDBY0_32    4
#define SPI_CLOCLPRESCALEDBY0_64    5
#define SPI_CLOCLPRESCALEDBY0_128   6
/* BitRatePara1*/
#define SPI_CLOCLPRESCALEDBY1_2     0
#define SPI_CLOCLPRESCALEDBY1_4     1
#define SPI_CLOCLPRESCALEDBY1_8     2
#define SPI_CLOCLPRESCALEDBY1_16    3
#define SPI_CLOCLPRESCALEDBY1_32    4
#define SPI_CLOCLPRESCALEDBY1_64    5
#define SPI_CLOCLPRESCALEDBY1_128   6

/*---------------------------------------------------------------------------*/

#endif  // C4MLIB_HAREWARE2_SPI_H
