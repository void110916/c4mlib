/**
 * @file uart_imp.h
 * @author
 * @date
 * @brief
 */

#ifndef C4MLIB_HARDWARE2_UART_IMP_H
#define C4MLIB_HARDWARE2_UART_IMP_H

#if defined(__AVR_ATmega128__)
#    include "m128/uart_imp.h"
#elif defined(__AVR_ATmega88__) || defined(__AVR_ATmega48__) || \
    defined(__AVR_ATmega168__)
#    include "m88/uart_imp.h"
#elif defined(__AVR_ATtiny2313__)
#    include "tiny2313/uart_imp.h"
#else
#    if !defined(__COMPILING_AVR_LIBC__)
#        warning "device type not defined"
#    endif
#endif

#endif  // C4MLIB_HARDWARE2_UART_IMP_H
