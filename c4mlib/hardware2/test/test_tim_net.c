#define USE_C4MLIB_DEBUG
#define F_CPU 11059200UL

#include "c4mlib/asabus/src/asabus.h"
#include "c4mlib/debug/src/debug.h"
#include "c4mlib/device/src/device.h"
#include "c4mlib/hardware2/src/tim.h"
#include "c4mlib/time/src/hal_time.h"

#include "c4mlib/config/tim.cfg"

int main(void) {
    C4M_DEVICE_set();

    printf("test_tim_net.c Start\n");
    TimIntStr_t tim0 = TIM0_INIT;
    TimIntStr_t tim1 = TIM1_INIT;
    TimIntStr_t tim2 = TIM2_INIT;
    TimIntStr_t tim3 = TIM3_INIT;


    TimInt_net(&tim0, 0);  // 連結變數 tim0 到 tiner0
    TimInt_set(&tim0);
    TimInt_net(&tim1, 1);  // 連結變數 tim1 到 tiner1
    TimInt_set(&tim1);
    TimInt_net(&tim2, 2);  // 連結變數 tim2 到 tiner2
    TimInt_set(&tim2);
    
    TimInt_net(&tim3, 3);  // 連結變數 tim3 到 tiner3
    TimInt_set(&tim3);

    printf("-- Show the register -- \n");

    printf("OCR0 = %x\n",OCR0);
    printf("TCCR0 = %x\n",TCCR0);
    printf("TCNT0 = %x\n",TCNT0);

    printf("OCR1A = %x\n",OCR1A);
    printf("TCCR1A = %x\n",TCCR1A);
    printf("TCCR1B = %x\n",TCCR1B);
    printf("TCCR1C = %x\n",TCCR1C);
    printf("TCNT1 = %x\n",TCNT1);
    
    printf("OCR2 = %x\n",OCR2);
    printf("TCCR2 = %x\n",TCCR2);
    printf("TCNT2 = %x\n",TCNT2);

    printf("OCR3A = %x\n",OCR3A);
    printf("TCCR3A = %x\n",TCCR3A);
    printf("TCCR3B = %x\n",TCCR3B);
    printf("TCCR3C = %x\n",TCCR3C);
    printf("TCNT3 = %x\n",TCNT3);
    
    return 0;
}
