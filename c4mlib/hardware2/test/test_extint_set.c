/**
 * @file test_extint_2.c
 * @author LiYu87
 * @brief 測試 ExtInt_set 是否能正常運作
 * @date 2019.11.14
 *
 * 測試 ExtInt_set 是否能正常運作，只觀察 INT7
 * 請把 A0 接到 INT7
 */

#include "c4mlib/device/src/device.h"
#include "c4mlib/hardware2/src/extint.h"

#include "c4mlib/config/ext.cfg"

ExtIntStr_t ExtInt7 = EXT_INIT;

typedef struct context {
    volatile uint16_t ext_id;
    volatile uint32_t counters;
} UserContext_t;

void func(void *);

int main() {
    C4M_STDIO_init();

    uint8_t res_net;
    uint8_t res_set;
    uint8_t res_en;
    uint8_t func_id;

    res_net = ExtInt_net(&ExtInt7, 7);
    res_set = ExtInt_set(&ExtInt7);

    func_id = ExtInt_reg(&ExtInt7, func, 0);
    
    ExtInt_en(&ExtInt7, func_id, ENABLE);

    printf("res of net is %d\n", res_net);
    printf("res of set is %d\n", res_set);
    printf("function id is %d\n", func_id);
    printf("res of net is %d\n", res_en);

    printf("DDRE = %d, EICRB = %d, EIMSK = %d\n", DDRE, EICRB, EIMSK);

    DDRA = 0x01;
    PORTA = 1;

    sei();

    while (1) {
        PORTA ^= 1;
        _delay_ms(1500);
    }
}

void func(void *p) {
    printf("enter isr !!\n");
}
