/**
 * @file test_pwmint.c
 * @author Deng Xiang-Guan
 * @date 2019.08.08
 * @brief
 * 
 * TODO: 完成並測試
 */

#include "c4mlib/asabus/src/pin_def.h"
#include "c4mlib/device/src/device.h"
#include "c4mlib/hardware2/src/pwm.h"

#include <avr/interrupt.h>
#include <avr/io.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>

PwmIntStr_t PwmInt0 = PWMINT_0_STR_INI;
PwmIntStr_t PwmInt1 = PWMINT_1_STR_INI;
PwmIntStr_t PwmInt2 = PWMINT_2_STR_INI;
PwmIntStr_t PwmInt3 = PWMINT_3_STR_INI;

typedef struct context0 {
    volatile uint32_t counter;
    volatile uint8_t duty_cycle_8_bits;
} UserContext_8_t;
typedef struct context1 {
    volatile uint32_t counter;
    volatile uint16_t duty_cycle_16_bits_A;
    volatile uint16_t duty_cycle_16_bits_B;
    volatile uint16_t duty_cycle_16_bits_C;
} UserContext_16_t;

/**
 * @note Please check PWM work secceed by using scope.
 * @PWM pin correspond to IO
 * OC0  : PB4
 * OC1A : PB5
 * OC1B : PB6
 * OC2  : PB7
 * OC3A : PE3
 * OC3B : PE4
 * OC3C : PE5
 */

/* User define function, when PWM interrupt, it will call. */
void USER_FUNCTION0(UserContext_8_t* pvData);
void USER_FUNCTION1(UserContext_16_t* pvData);
void USER_FUNCTION2(UserContext_8_t* pvData);
void USER_FUNCTION3(UserContext_16_t* pvData);


/* Setting PWM hardware function start. */
void init_pwm0(void);
void setPWM0Duty(uint8_t duty_cycle);
void init_pwm1(void);
void setPWM1ADuty(uint16_t duty_cycle);
void setPWM1BDuty(uint16_t duty_cycle);
void init_pwm2(void);
void setPWM2Duty(uint8_t duty_cycle);
void init_pwm3(void);
void setPWM3ADuty(uint16_t duty_cycle);
void setPWM3BDuty(uint16_t duty_cycle);
void setPWM3CDuty(uint16_t duty_cycle);
/* Setting PWM hardware function end. */

int main() {
    C4M_STDIO_init();
    ASABUS_ID_init();

    /***** Test the PWM interrupt*****/
    printf("======= Test PwmInt =======\n");

    // Define user context.
    UserContext_8_t PWM_context0 = {0};
    UserContext_16_t PWM_context1 = {0};
    UserContext_8_t PWM_context2 = {0};
    UserContext_16_t PWM_context3 = {0};

    /* Register the USER_FUNCTION to PWM interrupt, and PWM_contextn is the
     * parameters that user feed. */
    /* Register PWM0 */
    PwmInt_reg(&PwmInt0, (Func_t)USER_FUNCTION0, &PWM_context0);
    uint8_t pwm_id0 = PwmInt_net(&PwmInt0, 0);
    PwmInt_en(&PwmInt0, pwm_id0, true);
    /* Register PWM1 */
    PwmInt_reg(&PwmInt1, (Func_t)USER_FUNCTION1, &PWM_context1);
    uint8_t pwm_id1 = PwmInt_net(&PwmInt1, 1);
    PwmInt_en(&PwmInt1, pwm_id1, true);
    /* Register PWM2 */
    PwmInt_reg(&PwmInt2, (Func_t)USER_FUNCTION2, &PWM_context2);
    uint8_t pwm_id2 = PwmInt_net(&PwmInt2, 2);
    PwmInt_en(&PwmInt2, pwm_id2, true);
    /* Register PWM3 */
    PwmInt_reg(&PwmInt3, (Func_t)USER_FUNCTION3, &PWM_context3);
    uint8_t pwm_id3 = PwmInt_net(&PwmInt3, 3);
    PwmInt_en(&PwmInt3, pwm_id3, true);

    printf("Enable Global Interrupt\n");
    /* Hardware initialization. */
    init_pwm0();
    init_pwm1();
    init_pwm2();
    init_pwm3();
    sei();

    printf("Run main \n");
    while (true) {
        // printf("TCNT0:%d, TCCR0:%x, TIFR:%x\n", TCNT0, TCCR0, TIFR);
        // printf("duty0:%d, counters0:%lu, duty1A:%d, duty1B:%d, counters1:%lu\n", PWM_context0.duty_cycle_8_bits, PWM_context0.counter,
        // PWM_context1.duty_cycle_16_bits_A, PWM_context1.duty_cycle_16_bits_B, PWM_context1.counter);
        printf("duty2:%d, counters2:%lu, duty3A:%d, duty3B:%d, duty3C:%d, counters1:%lu\n", PWM_context2.duty_cycle_8_bits, PWM_context2.counter,
        PWM_context3.duty_cycle_16_bits_A, PWM_context3.duty_cycle_16_bits_B, PWM_context3.duty_cycle_16_bits_C, PWM_context3.counter);
    }
}

void USER_FUNCTION0(UserContext_8_t* pvData) {
    pvData->counter++;
    if(pvData->counter % 50 == 0) {
        pvData->duty_cycle_8_bits++;
    }
    setPWM0Duty(pvData->duty_cycle_8_bits);
}
void USER_FUNCTION1(UserContext_16_t* pvData) {
    pvData->counter++;
    pvData->duty_cycle_16_bits_A++;
    if(pvData->counter % 2 == 0) {
        pvData->duty_cycle_16_bits_B++;
    }
    if(pvData->counter % 3 == 0) {
        pvData->duty_cycle_16_bits_C++;
    }
    setPWM1ADuty(pvData->duty_cycle_16_bits_A);
    setPWM1BDuty(pvData->duty_cycle_16_bits_B);
}
void USER_FUNCTION2(UserContext_8_t* pvData) {
    pvData->counter++;
    if(pvData->counter % 50 == 0) {
        pvData->duty_cycle_8_bits++;
    }
    setPWM2Duty(pvData->duty_cycle_8_bits);
}
void USER_FUNCTION3(UserContext_16_t* pvData) {
    pvData->counter++;
    pvData->duty_cycle_16_bits_A++;
    if(pvData->counter % 2 == 0) {
        pvData->duty_cycle_16_bits_B++;
    }
    if(pvData->counter % 3 == 0) {
        pvData->duty_cycle_16_bits_C++;
    }
    setPWM3ADuty(pvData->duty_cycle_16_bits_A);
    setPWM3BDuty(pvData->duty_cycle_16_bits_B);
    setPWM3CDuty(pvData->duty_cycle_16_bits_C);
}

/* PWM Hardware initialize begin */
void init_pwm0(void) {
    /**
     * Open PWM0
     * Prescaler:1024
     * OC0:PB4
     * TIMER0 overflow interrupt enable.
     */
    DDRB |= (1<<PB4);
    TCCR0 = 0;
    TCCR0 |= (1<<WGM00)|(1<<WGM01)|(1<<CS01)|(1<<CS02)|(1<<COM01);
    TIMSK |= (1<<TOIE0);
}
void setPWM0Duty(uint8_t duty_cycle) {
    OCR0 = duty_cycle;
}
void init_pwm1(void) {
    /**
     * Open PWM1 A、B channel
     * Prescaler:1
     * OC1A:PB5
     * OC1B:PB6
     * TIMER1 overflow interrupt enable.
     */
    DDRB |= (1<<PB5)|(1<<PB6);
    TCCR1A = 0;
    TCCR1B = 0;
    TCCR1A |= (1<<COM1A1)|(1<<COM1B1)|(1<<WGM11);
    TCCR1B |= (1<<WGM12)|(1<<WGM13)|(1<<CS10);
    TIMSK |= (1<<TOIE1);
    ICR1 = INT16_MAX;
}
void setPWM1ADuty(uint16_t duty_cycle) {
    OCR1A = duty_cycle;
}
void setPWM1BDuty(uint16_t duty_cycle) {
    OCR1B = duty_cycle;
}

void init_pwm2(void) {
    /**
     * Open PWM2
     * Prescaler:1024
     * OC0:PB7
     * TIMER2 overflow interrupt enable.
     */
    DDRB |= (1<<PB7);
    TCCR2 = 0;
    TCCR2 |= (1<<WGM20)|(1<<WGM21)|(1<<CS20)|(1<<CS22)|(1<<COM21);
    TIMSK |= (1<<TOIE2);
}
void setPWM2Duty(uint8_t duty_cycle) {
    OCR2 = duty_cycle;
}
void init_pwm3(void) {
    /**
     * Open all PWM3 channel
     * Prescaler:1
     * OC3A:PE3
     * OC3B:PE4
     * OC3C:PE5
     * TIMER1 overflow interrupt enable.
     */
    DDRE |= (1<<PE3)|(1<<PE4)|(1<<PE5);
    TCCR3A = 0;
    TCCR3B = 0;
    TCCR3A |= (1<<COM3A1)|(1<<COM3B1)|(1<<COM3C1)|(1<<WGM31);
    TCCR3B |= (1<<WGM32)|(1<<WGM33)|(1<<CS30);
    ETIMSK |= (1<<TOIE3);
    ICR3 = INT16_MAX;
}
void setPWM3ADuty(uint16_t duty_cycle) {
    OCR3A = duty_cycle;
}
void setPWM3BDuty(uint16_t duty_cycle) {
    OCR3B = duty_cycle;
}
void setPWM3CDuty(uint16_t duty_cycle) {
    OCR3C = duty_cycle;
}

/* PWM Hardware initialize end */
