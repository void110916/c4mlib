/**
 * @file test_tim1_int.c
 * @author Yee-Mo
 * @brief 提供timer interrupt測試程式
 * @date 2019-09-05
 */
#include "c4mlib/asabus/src/asabus.h"
#include "c4mlib/debug/src/debug.h"
#include "c4mlib/device/src/device.h"
#include "c4mlib/hardware2/src/tim.h"
#include "c4mlib/time/src/hal_time.h"

#include "c4mlib/config/tim.cfg"

TimIntStr_t TimInt1 = TIM1_INIT;

void USER_FUNCTION(void* pvData_p);

int main(void) {
    C4M_DEVICE_set();

    TimInt_net(&TimInt1, 1);  // 連結變數 TimInt1 到 timer1
    TimInt_set(&TimInt1);

    uint8_t counter1 = 0;
    uint8_t Timint_id = TimInt_reg(&TimInt1, USER_FUNCTION, &counter1); //登入函式到TimInt

    TimInt_en(&TimInt1, Timint_id, true);

    printf("Enable Global Interrupt\n");
    /** Enable interrupts */
    sei();
    while(1){
        printf("TCNT1 = %x\t,TIMSK = %x\t,counter=%d\n",TCNT1,TIMSK,counter1);
    }
}

void USER_FUNCTION(void* pvData_p) {
    *(uint8_t*)pvData_p=*(uint8_t*)pvData_p+1;
}
