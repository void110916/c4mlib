/**
 * @file test_slave_twiint.c
 * @author maxwu84
 * @date 2019.09.06
 * @brief 提供ASA函式庫標準中斷介面測試程式，請確認printf出來的succeed_cnt持續增加。
 */
#define USE_C4MLIB_DEBUG
#define F_CPU 11059200UL

#include "c4mlib/asabus/src/pin_def.h"
#include "c4mlib/debug/src/debug.h"
#include "c4mlib/device/src/device.h"
#include "c4mlib/hardware2/src/twi.h"
#include "c4mlib/time/src/hal_time.h"
#include "c4mlib/config/twi.cfg"

#include <avr/interrupt.h>
#include <avr/io.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>

#define TWI_SLAVE_ID 0x78

#define USE_TWI_ISR_ID 0

TwiIntStr_t TwiInt0 = TWI_INIT;

typedef struct context {
    const uint8_t addr;
    uint8_t data;
    uint8_t cnt;
} UserContext_t;

/* User define function, when TWI interrupt, it will call. */
void USER_FUNCTION(UserContext_t* user_context);

/* Setting TWI hardware function start. */
/* Setting TWI hardware function end. */

int main(void) {
    C4M_STDIO_init();
    ASABUS_ID_init();

    /***** Test the TWI interrupt*****/
    printf("======= Test TWI interrupt =======\n");
    printf("Please press salve reset first, and then press master reset.\n");

    // Define user context.
    UserContext_t TWI_context = {TWI_SLAVE_ID, 0, 0};

    printf(" ---- initial START ---- \n");
    TwiInt_net(&TwiInt0, USE_TWI_ISR_ID);
    TwiInt_set(&TwiInt0);
    uint8_t id1 = TwiInt_reg(&TwiInt0, (Func_t)USER_FUNCTION, &TWI_context);
    TwiInt_en(&TwiInt0, id1, true);
    printf(" ---- initial END ---- \n");
    
    printf(" ---- Show thr register START ---- \n");
    printf("TWAR = %x\n",TWAR);
    printf("TWBR = %x\n",TWBR); 
    printf("TWCR = %x\n",TWCR);

    printf(" ---- Show thr register END ---- \n");
    
    sei();

    while(true) {
        printf("true\n");
    }
}

void USER_FUNCTION(UserContext_t* user_context) { 
    DEBUG_INFO("Run user function !!!\n");
}
