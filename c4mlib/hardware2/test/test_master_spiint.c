/**
 * @file test_master_spiint.c
 * @author Deng Xiang-Guan
 * @date 2019.08.08
 * @brief 提供ASA函式庫標準中斷介面測試程式，Master SPI輸出data 給Slave。
 */

#include "c4mlib/asabus/src/pin_def.h"
#include "c4mlib/debug/src/debug.h"
#include "c4mlib/device/src/device.h"
#include "c4mlib/hardware2/src/spi.h"

#include <avr/interrupt.h>
#include <avr/io.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>

#define ASA_ID 4

SpiIntStr_t SpiInt0 = SPIINT_0_STR_INI;

typedef struct context {
    uint8_t addr;
    uint8_t data[2];
    uint8_t cnt;
} UserContext_t;

UserContext_t SPI_context = {0, {0, 0}, 3};

/* Setting SPI hardware function start. */
void init_spi(void);
void cs_enable(void);
void cs_disable(void);
/* Setting SPI hardware function end. */

int main() {
    C4M_STDIO_init();
    ASABUS_ID_init();
    ASABUS_ID_set(ASA_ID);

    printf("Enable SPI master\n");
    /** Initialize SPI hardware registers as master mode. */
    init_spi();

    uint8_t sendData = 0;
    while (1) {
        DEBUG_INFO("Send SPI testing data\n");
        cs_enable();
        ASABUS_SPI_swap(0xaa);
        ASABUS_SPI_swap(sendData++);
        ASABUS_SPI_swap(sendData++);
        cs_disable();
        DEBUG_INFO("Send OK\n");
        _delay_ms(3000);
    }
}

// Initialize SPI hardware as master.
void init_spi(void) {
    // Initialize ASA SPI ID
    ASABUS_ID_init();
    // Setup SPI pins
    DDRB |= (1 << BUS_SPI_MOSI) | (1 << BUS_SPI_SCK) | (1 << BUS_SPI_SS);
    DDRB &= ~(1 << BUS_SPI_MISO);
    // Master CS pin is PF4
    ASA_CS_DDR |= (1 << ASA_CS);
    // SCK oscillator frequency divided 128
    // SPI Master mode
    // Enable SPI
    SPCR |= (1 << SPR0) | (1 << SPR1) | (1 << MSTR) | (1 << SPE);
}

void cs_enable(void) { ASA_CS_PORT |= (1 << ASA_CS); }

void cs_disable(void) { ASA_CS_PORT &= ~(1 << ASA_CS); }
