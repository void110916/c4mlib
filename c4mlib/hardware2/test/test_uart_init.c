/**
 * @file test_uart_init.c
 * @author LiYu87
 * @date 2019.09.05
 * @brief
 *
 * 確認 UART 0 1 相關暫存器有改變，確認 set 與 initFunc 有正常連接。
 * 其中 UART 0 也為 ASA_M128_V2 對PC端所使用的硬體，所以若設置錯誤、
 * 或函式出錯，則無法與PC通訊。
 * 
 * 測試裝置：ASA_M128_V2
 * 測試硬體：UART 0 1
 */

#include "c4mlib/device/src/device.h"
#include "c4mlib/hardware2/src/uart.h"

#include "c4mlib/config/uart.cfg"

int main(void) {
    C4M_DEVICE_set();

    printf("uart init test start -----------------------------------------\n");

    UartIntStr_t uart0 = UART_INIT;
    UartIntStr_t uart1 = UART_INIT;

    UartInt_net(&uart0, 0);
    UartInt_set(&uart0);

    UartInt_net(&uart1, 1);
    UartInt_set(&uart1);

    printf("UCSR0A = 0x%02x\n", UCSR0A);
    printf("UCSR0B = 0x%02x\n", UCSR0B);
    printf("UCSR0C = 0x%02x\n", UCSR0C);
    printf("UBRR0H = 0x%02x\n", UBRR0H);
    printf("UBRR0L = 0x%02x\n", UBRR0L);

    printf("UCSR1A = 0x%02x\n", UCSR1A);
    printf("UCSR1B = 0x%02x\n", UCSR1B);
    printf("UCSR1C = 0x%02x\n", UCSR1C);
    printf("UBRR1H = 0x%02x\n", UBRR1H);
    printf("UBRR1L = 0x%02x\n", UBRR1L);

    return 0;
}
