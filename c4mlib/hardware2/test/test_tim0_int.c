/**
 * @file test_tim0_int.c
 * @author Yee-Mo
 * @brief 提供timer interrupt測試程式
 * @date 2019-09-05
 */
#include "c4mlib/asabus/src/asabus.h"
#include "c4mlib/debug/src/debug.h"
#include "c4mlib/device/src/device.h"
#include "c4mlib/hardware2/src/tim.h"
#include "c4mlib/time/src/hal_time.h"

#include "c4mlib/config/tim.cfg"

TimIntStr_t TimInt0 = TIM0_INIT;

void USER_FUNCTION(void* pvData_p);

int main(void) {
    C4M_DEVICE_set();

    printf("Initial Start\n");
    TimInt_net(&TimInt0, 0);  // 連結變數 TimInt0 到 tiner0
    TimInt_set(&TimInt0);

    uint8_t counter0 = 0;
    uint8_t Timint_id = TimInt_reg(&TimInt0, USER_FUNCTION, &counter0);
    printf("Added USER_FUNCTION with fg_interrupt0  to TimInt0 Component\n");

    TimInt_en(&TimInt0, Timint_id, true);

    printf("Enable Global Interrupt\n");
    /** Enable interrupts */
    sei();
    while(1){
        printf("OCR0 = %x\t,TCCR0 = %x\t,TCNT0 = %x\t,TIMSK = %x\t,counter=%d\n",OCR0,TCCR0,TCNT0,TIMSK,counter0);
    }
}

void USER_FUNCTION(void* pvData_p) {
    *(uint8_t*)pvData_p=*(uint8_t*)pvData_p+1;
}
